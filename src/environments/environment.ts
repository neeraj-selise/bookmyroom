// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAGv-WBjIZGjLatbnzSxDmAkUQBLjrgb_A',
    authDomain: 'selise-booking-2020.firebaseapp.com',
    databaseURL: 'https://selise-booking-2020.firebaseio.com',
    projectId: 'selise-booking-2020',
    storageBucket: 'selise-booking-2020.appspot.com',
    messagingSenderId: '923737463472',
    appId: '1:923737463472:web:709bc90b72dce61c77f975',
    measurementId: 'G-21W5G001PQ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
