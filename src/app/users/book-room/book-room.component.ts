import { Component, OnInit } from '@angular/core';
import {RoomBookService} from '../../shared/Services/room-book.service';
import {MatDialog} from '@angular/material';
import {BookModalComponent} from './book-modal/book-modal.component';

@Component({
  selector: 'app-book-room',
  templateUrl: './book-room.component.html',
  styleUrls: ['./book-room.component.css']
})
export class BookRoomComponent implements OnInit {

  rooms = [
    {
      name: 'ThaRingSai SaKhong',
      location: 'REST',
      maxPeople: 12,
      minPeople: 4,
      description: 'The root of SELISE Bhutan. Interviews and Seminars are held here.',
      imgUrl: '../../../assets/Image/M1.jpg'
    },
    {
      name: 'NyamGaWai SaKhong',
      location: 'Graph QL',
      maxPeople: 14,
      minPeople: 5,
      description: 'Home to the residents. Best place for scrums and video conferencing.',
      imgUrl: '../../../assets/Image/M2.jpg'
    },
    {
      name: 'NaLongLo SaKhong',
      location: 'Graph QL',
      maxPeople: 8,
      minPeople: 1,
      description: 'Best suited for client video or voice calls.',
      imgUrl: '../../../assets/Image/M3.jpg'
    }
  ];
  dialogResult = '';

  constructor(private roomService: RoomBookService,
              private dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog() {
    const dialogRef = this.dialog.open(BookModalComponent, {
      width: '600px',
      data: this.rooms,
    } );
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

}
