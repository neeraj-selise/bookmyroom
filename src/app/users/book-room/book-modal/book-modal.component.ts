import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {RoomBookService} from '../../../shared/Services/room-book.service';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-book-modal',
  templateUrl: './book-modal.component.html',
  styleUrls: ['./book-modal.component.css']
})
export class BookModalComponent implements OnInit {

  attendees: FormArray;

  constructor(public dialogRef: MatDialogRef<BookModalComponent>, private fb: FormBuilder, private bookService: RoomBookService) { }

  bookingForm: FormGroup;
  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.bookingForm = this.fb.group({
      eventName: [''],
      name: [''],
      location: [''],
      date: [''],
      startTime: [''],
      endTime: [''],
      host: [''],
      attendeeName: this.fb.array(
        [ this.createAttendeeName() ] )
    });
  }
  createAttendeeName() {
    return this.fb.group({
      name: ['']
    });
  }

  addAttendees(): void {
    this.attendees = this.bookingForm.get('attendeeName') as FormArray;
    this.attendees.push(this.createAttendeeName());
  }

  deleteAttendees(i): void {
    this.attendees = this.bookingForm.get('attendeeName') as FormArray;
    this.attendees.removeAt(i);
  }

  saveBook() {
    this.bookService.bookRoom(this.bookingForm.value).then(result => {
      this.dialogRef.close();
    });
  }

}
