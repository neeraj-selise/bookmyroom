import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookRoomComponent } from './book-room.component';
import {BookModalComponent} from './book-modal/book-modal.component';

const routes: Routes = [
  {
    path: '',
    component: BookRoomComponent
  },
  {
    path: 'book-modal',
    component: BookModalComponent
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookRoomRoutingModule { }
