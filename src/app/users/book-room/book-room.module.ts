import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookRoomRoutingModule } from './book-room-routing.module';
import { BookRoomComponent } from './book-room.component';
import { BookModalComponent } from './book-modal/book-modal.component';
import {MatButtonModule, MatCardModule, MatDialogModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RoomBookService} from '../../shared/Services/room-book.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
    declarations: [BookRoomComponent, BookModalComponent],
    exports: [
        BookRoomComponent
    ],
  imports: [
    CommonModule,
    BookRoomRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [RoomBookService],
  entryComponents: [BookModalComponent],
})
export class BookRoomModule { }
