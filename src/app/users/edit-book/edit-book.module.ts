import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditBookRoutingModule } from './edit-book-routing.module';
import { EditBookComponent } from './edit-book.component';
import {MatButtonModule, MatTableModule} from '@angular/material';


@NgModule({
  declarations: [EditBookComponent],
  exports: [EditBookComponent],
    imports: [
        CommonModule,
        EditBookRoutingModule,
        MatTableModule,
        MatButtonModule
    ]
})
export class EditBookModule { }
