import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'book-room',
    loadChildren: () => import('./book-room/book-room.module').then(m => m.BookRoomModule)
  },
  {
    path: 'edit-book',
    loadChildren: () => import('./edit-book/edit-book.module').then(m => m.EditBookModule)
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
