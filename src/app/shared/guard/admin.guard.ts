import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import {AuthService} from '../Services/auth.service';
import {map, switchMap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private authService: AuthService) {
  }
  canActivate(): Observable<boolean> {
    return this.authService.user$
      .pipe(switchMap(user => of(this.authService.get(user.uid))))
        .pipe(map(AppUser => AppUser.isAdmin));
  }

}
