import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PasswordValidator} from '../Validators/password.validator';
import {AuthService} from '../Services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  // Form Component Shortening
  get firstName() {
    return this.registrationForm.get('firstName');
  }

  get lastName() {
    return this.registrationForm.get('lastName');
  }

  get email() {
    return this.registrationForm.get('eMail');
  }

  get password() {
    return this.registrationForm.get('passWord');
  }

  get confirmPassword() {
    return this.registrationForm.get('confirmPassword');
  }

  constructor(
    private fb: FormBuilder,
    public authService: AuthService) { }

  registrationForm: FormGroup;

  ngOnInit() {
    this.registrationForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      eMail: ['', Validators.required],
      passWord: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, {validator: PasswordValidator});
  }
}
