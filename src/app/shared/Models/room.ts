export interface Room {
  id?: number;
  name?: string;
  location?: string;
  maxPeople?: number;
  minPeople?: number;
  description?: string;
}
