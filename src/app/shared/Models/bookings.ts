export interface Bookings {
  id?: number;
  name?: string;
  location?: string;
  date?: string;
  startTime?: string;
  endTime?: string;
  event?: string;
  host?: string;
  attendees?: string;
}
