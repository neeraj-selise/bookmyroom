import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { CalendarComponent } from './calendar/calendar.component';
import {FullCalendarModule} from '@fullcalendar/angular';


@NgModule({
    declarations: [HomeComponent, CalendarComponent],
    exports: [
      HomeComponent
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        FullCalendarModule
    ]
})
export class HomeModule { }
