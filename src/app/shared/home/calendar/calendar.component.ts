/* tslint:disable:no-trailing-whitespace */
import { Component, OnInit, ViewChild } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import {OptionsInput} from '@fullcalendar/core';
import {FullCalendarComponent} from '@fullcalendar/angular';
import timeGridPlugin from '@fullcalendar/timegrid';
import list from '@fullcalendar/list';
import bootstrapPlugin from '@fullcalendar/bootstrap';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  options: OptionsInput;
  eventsModel: any;

  @ViewChild('fullCalendar', { static: false }) fullCalendar: FullCalendarComponent;

  ngOnInit() {
    this.options = {
      editable: true,
      customButtons: {
        myCustomButton: {
          text: 'Custom',
          click() {
            confirm('Are you sure?');
          }
        }
      },
      buttonText: {
        today: 'Today',
        prev: 'Prev',
        prevYear: 'Prev Year',
        nextYear: 'Next Year',
        next: 'Next',
        month: 'Monthly View',
        week: 'Weekly View',
        list: 'Event List',
        day: 'Daily View'
      },
      header: {
        left: 'prevYear,prev,next,nextYear myCustomButton',
        center: 'title',
        right: 'today dayGridMonth,timeGridWeek,timeGridDay,list'
      },
      footer: {
        left: '',
        center: '&copy; Neeraj Pradhan 2020',
        right: ''
      },
      plugins: [dayGridPlugin, interactionPlugin, timeGridPlugin, list, bootstrapPlugin]
    };
  }

  eventClick(model) {
    console.log(model);
  }

  eventDragStop(model) {
    console.log(model);
  }

  dateClick(model) {
    console.log(model);
  }

  updateHeader() {
    this.options.header = {
      left: 'prev,next myCustomButton',
      center: 'title',
      right: ''
    };
  }

  updateEvents() {
    this.eventsModel = [{
      title: 'Update Event',
      start: this.yearMonth,
      end: this.yearMonth
    }];
  }

  get yearMonth(): string {
    const dateObj = new Date();
    return dateObj.getFullYear() + '-' + (dateObj.getMonth() + 1);
  }

}
