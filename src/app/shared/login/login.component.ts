import { Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../Services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // Form Component Shortening

  get email() {
    return this.loginForm.get('eMail');
  }

  get password() {
    return this.loginForm.get('passWord');
  }

  constructor(
    private fb: FormBuilder,
    public authService: AuthService,
    private router: Router) {
    authService.user$.subscribe(user => {
      if (user) {
        authService.save(user);
        const returnUrl = localStorage.getItem('returnUrl');
        router.navigateByUrl(returnUrl);
      }
    });
  }

  loginForm: FormGroup; // loginForm Group Definition

  ngOnInit() {
    this.loginForm = this.fb.group({
      eMail: ['', Validators.required],
      passWord: ['', Validators.required]
    }); // loginForm Builder
  }

}
