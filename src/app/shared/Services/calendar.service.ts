import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {AngularFirestore} from '@angular/fire/firestore';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  constructor(private db: AngularFirestore) { }

  getEvents(): Observable<any[]> {
    return this.db.collection('/userBookings/').valueChanges().pipe(
      tap(events => console.log(events)),
      map(events => events.map(event => {
        const data: any = event;
        data.date = data.date.toLocaleDateString();
        return data;
      }))
    );
  }
}
