import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class RoomBookService {

  constructor(private db: AngularFireDatabase) {}

  bookRoom(userBooks) {
    return this.db.list('/userBookings').push(userBooks);
  }

}
