import { TestBed } from '@angular/core/testing';

import { RoomBookService } from './room-book.service';

describe('RoomBookService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoomBookService = TestBed.get(RoomBookService);
    expect(service).toBeTruthy();
  });
});
