import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule, MatSidenavModule, MatToolbarModule} from '@angular/material';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AuthService} from './shared/Services/auth.service';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AuthGuard} from './shared/guard/auth.guard';
import {AngularFireDatabase} from '@angular/fire/database';
import {AdminGuard} from './shared/guard/admin.guard';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    MatSidenavModule,
    AngularFireAuthModule
  ],
  providers: [AuthService, AuthGuard, AngularFireDatabase, AdminGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
