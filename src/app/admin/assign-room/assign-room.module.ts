import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssignRoomRoutingModule } from './assign-room-routing.module';
import { AssignRoomComponent } from './assign-room.component';
import { ViewBooksComponent } from './view-books/view-books.component';
import {MatButtonModule} from '@angular/material';


@NgModule({
    declarations: [AssignRoomComponent, ViewBooksComponent],
    exports: [
        AssignRoomComponent
    ],
  imports: [
    CommonModule,
    AssignRoomRoutingModule,
    MatButtonModule
  ]
})
export class AssignRoomModule { }
