import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssignRoomComponent } from './assign-room.component';
import {ViewBooksComponent} from './view-books/view-books.component';

const routes: Routes = [
  {
    path: '',
    component: AssignRoomComponent
  },
  {
    path: 'view-books',
    component: ViewBooksComponent
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignRoomRoutingModule { }
